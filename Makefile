### -*-Makefile-*- to build the site of "Roger the Omni Grader"
##
## Copyright (C) 2021 Vincent Goulet
##
## Author: Vincent Goulet
##
## This file is part of Roger <https://gitlab.com/roger-project>.


## Package name and CRAN url
MAIN := roger
CRANURL := https://cran.r-project.org/package=${MAIN}

## GitLab repository and authentication
REPOSNAME := ${MAIN}-base
HEAD := master
APIURL := https://gitlab.com/api/v4/projects/roger-project%2F${REPOSNAME}
OAUTHTOKEN := $(shell cat ~/.gitlab/token)

## Extract the version info of roger-base from the main script source
## file as published in branch master of the repository
VERSIONBASEFULL := $(shell \
  curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
       --silent \
       "${APIURL}/repository/files/src%2F${MAIN}.sh/raw?ref=${HEAD}" | \
  awk 'BEGIN { FS = "=" } \
       /^VERSION=/ { version = $$2 } \
       /^DATE=/ { date = $$2 } \
       END { print version " \\(" date "\\)" }')
VERSIONBASE := $(word 1,${VERSIONBASEFULL})
TAGNAME = v${VERSIONBASE}

## Get the url of assets published with the release of roger-base
ASSETS := $(shell \
  curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
       --silent \
       ${APIURL}/releases/${TAGNAME}/assets/links | \
  sed 's/,/\n/g' | \
  awk 'BEGIN { FS = "\"" } \
       /direct_asset_url/ \
       { \
         sub(/.*\/uploads/, "uploads", $$4); \
         f = f " " $$4 \
       } \
       END { print f }')
PKG_ID := $(filter %.pkg,${ASSETS})
EXE_ID := $(filter %.exe,${ASSETS})
ZIP_ID := $(filter %.zip,${ASSETS})

## Extract the version info of the R package from CRAN.
VERSIONRPKGFULL := $(shell \
  curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
       --silent --location \
       "${CRANURL}" | \
  pandoc -f html -t plain | \
  awk '/^  Version:/ { version = $$2 } \
       /^  Published:/ { date = $$2 } \
       END { print version " (" date ")" }')


all: files commit

files:
	awk 'BEGIN { FS = "\""; OFS = "\"" } \
	     /version_base/ { $$2 = "${VERSIONBASEFULL}" } \
	     /pkg_id/ { $$2 = "${PKG_ID}" } \
	     /exe_id/ { $$2 = "${EXE_ID}" } \
	     /zip_id/ { $$2 = "${ZIP_ID}" } \
	     /version_rpkg/ { $$2 = "${VERSIONRPKGFULL}" } \
	     1' \
	    config.toml > tmpfile && \
	  mv tmpfile config.toml

commit:
	git commit config.toml \
	    -m "Updated site for v${VERSIONBASE} of roger-base and v${VERSIONRPKG} of roger-rpkg"; \
	git push
