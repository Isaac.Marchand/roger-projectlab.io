---
title: Roger the Omni Grader
description: Automated grading system for R scripts... and much more
---

# Presentation

Roger is an automated grading system for computer programming
projects. Although specialized for R scripts, Roger can easily be
adapted to grade code in other interpreted or compiled programming
languages, thanks to its flexibility and modularity.

At the root of the system, Roger relies on Unix shell scripts to grade
the output correctness of a program. The companion R package provides
additional tools to grade the coding style and documentation of R scripts.

Roger is actively used for grading at 
[École d'actuariat](https://act.ulaval.ca) of 
[Université Laval](https://ulaval.ca).

# Features

The system is based on the concept of grading criteria such as the
[Computer Science Program Grading Criteria](https://wiki.cs.astate.edu/index.php?title=Computer_Science_Program_Grading_Criteria&oldid=10507)
of Arkansas State University. Roger takes care of the objective
criteria such as output correctness, coding style or the presence of
certain documentation elements (assessing their content and quality
documentation is left to humans).

- **Multiplatform** --- the base system is build on Bash shell
  scripts and standard Unix tools. (Requires [MSYS2](https://msys2.org) or [Git
  Bash](https://git-scm.com/download/win) on Windows.)
  
- Extensive support for projects administered and handed in via **Git
  repositories** --- bulk repository cloning; validation and
  grading of the structure of the repositories; posting of the results
  directly in the repositories.
  
- Versatile grading system based on **unit tests** --- you provide
  the tests, Roger takes care of their execution and compiles the
  results.
  
- **Flexible** structure to grade any number of files for any number
  of characteristics, each for any number of grading criteria ---
  you create the "grading map" using a simple syntax
  not unlike [YAML](https://fr.wikipedia.org/wiki/YAML) and Roger
  takes care of the rest.
  
- Project validation using the included **validation script** ---
  students can check that their project meets the requirements *before*
  they hand it their work. Students and instructors save a lot of
  tears and unnecessary stress.

- Integrated grading of R scripts, R&nbsp;Markdown documents,
  Shiny apps and shell scripts.
  
- Checks on the integrity of every file: name, contents, character
  encoding, code validity.
  
- **Multilingual** --- the system is currently entirely available
  in English and French.

# Components

Roger is formed of two independent components.

- [`roger-base`]({{< relref path="download.md" lang="fr" >}}) provides
  the shell scripts that form the foundations of the system.
  
- The R package [**roger**]({{< param cran >}}) provides an interface
  to the shell scripts and tools to grade the coding style and
  documentation of R scripts.

# Authors

- [David Beauchemin](https://github.com/davebulaval): ideation and
  first grading scripts.

- Samuel Fréchette: R interface to the shell scripts.

- [Vincent Goulet](https://vigou3.gitlab.io): shell scripts and
  maintainer of the project.
  
- Jean-Christophe Langlois: R package linters.

# License

Roger is Free Software distributed under the [GNU GPL](https://www.gnu.org/licenses/gpl-3.0.html) version 2 or
above.

# Contributing

Contributions are welcome for new functionalities, extensions to other
programming languages, translations, or simply bug report. [Join us on
GitLab](https://gitlab.com/roger-project).
