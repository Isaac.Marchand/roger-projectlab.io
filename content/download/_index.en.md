---
title: Downloads
---

# Base System

The package `roger-base` contains the Unix shell scripts at the
cornerstone of the automated grading system.

The latest release is **{{< param version_base >}}**.
{{< release_notes >}}

{{< macos-button >}}
{{< windows-button >}}

If you are on Linux or if you prefer a manual installation, use the
ZIP archive option below. Uncompress the archive into the root of the
filesystem.

{{< zip-button >}}

> Roger requires gettext to translate messages. You already have the
> software if you installed [Git](https://git-scm.com/downloads),
> notably on Windows. On macOS, just make sure to install Git through
> [Homebrew](https://brew.sh) and you are good to go.

The base system contains the following scripts.

- `roger` is the unified interface to the tools below.

- `roger validate` allows students to validate their work (file and
  function names, structure of the Git repository, validity of code)
  prior to handing it in.
  
- `roger checkreq` checks that all required software (command line
  utilities, R packages) is installed on a grader's workstation prior
  to grading.
  
- `roger clone` fetches at once the students projects hosted in Git
  repositories.
  
- `roger grade` is the true workhorse that grades the projects for the
  criteria that can be handled automatically.

- `roger push` sends the grading sheet to students directly in their
  Git repository once grading is complete.


# R Package

The R package **roger** provides an interface to the shell scripts and
tools to grade the coding style and documentation of R scripts. It is
distributed exclusively through
[CRAN](https://cran.r-project.org/package=roger).

The latest release is **{{< param version_rpkg >}}**.
{{< cran_news >}}

Install the package directly from R:

```R
> install.packages("roger")
```

# Source Code

The source code of Roger the Omni Grader is hosted on GitLab.

{{< source-button >}}
