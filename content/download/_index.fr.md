---
title: Téléchargements
---

# Système de base

Le paquetage `roger-base` contient les scripts d'interpréteur de
commandes (*shell scripts*) Unix à la base du système de correction
automatisé.

La plus récente version est **{{< param version_base >}}**. 
{{< release_notes >}}

{{< macos-button >}}
{{< windows-button >}}

Sous Linux ou pour installer les scripts manuellement, télécharger
l'archive ZIP ci-dessous. Extraire le contenu de l'archive à la racine
de votre système de fichiers.

{{< zip-button >}}

> Roger requiert gettext pour la traductions des messages. Vous
> disposez déjà de l'outil si vous avez installé
> [Git](https://git-scm.com/downloads), notamment sous Windows. Sous
> macOS, assurez-vous simplement d'installer Git avec
> [Homebrew](https://brew.sh/index_fr) et le tour est joué.

Le système de base est formé des outils suivants.

- `roger` est l'interface unifiée vers les autres outils ci-dessous.

- `roger validate` est le script de validation qui permet aux
  étudiants de valider leur travail (noms des fichiers et des
  fonctions, structure du dépôt Git, validité du code informatique)
  avant d'en effectuer la remise.
  
- `roger checkreq` vérifie la disponibilité des outils de correction
  (utilitaires en ligne de commande, paquetages R) sur le poste de
  travail de la personne effectuant la correction avant de lancer
  celle-ci.
  
- `roger clone` clone en lot tous les travaux d'étudiants hébergés
  dans des dépôts Git.
  
- `roger grade` est le cœur du système, l'outil qui se charge de la
  correction automatique pour les critères qui le permettent.

- `roger push` publie les résultats directement dans le dépôt Git des
  étudiants une fois la correction complétée.


# Paquetage R

Le paquetage R **roger** fournit une interface pour les scripts ainsi
que les outils requis pour corriger le style de codage et la
documentation dans les scripts R. Il est distribué exclusivement via
[CRAN](https://cran.r-project.org/package=roger).

La plus récente version est **{{< param version_rpkg >}}**. 
{{< cran_news >}}

Installer le paquetage directement depuis R:

```R
> install.packages("roger")
```

# Code source

Le code source de Roger l'omnicorrecteur est hébergé dans GitLab.

{{< source-button >}}

