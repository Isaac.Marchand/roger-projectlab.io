---
title: Roger l'omnicorrecteur
description: Système de correction automatisée pour les scripts R... et bien plus
---

# Présentation

Roger est un système de correction automatisée pour les travaux
informatiques. Spécialisé dans la correction de code R, Roger peut
être adapté à d'autres langages de programmation interprétés ou
compilés grâce à sa polyvalence et à sa modularité.

À la base du système, Roger utilise des procédures d’interpréteur de
commandes (*shell scripts*) Unix pour corriger la justesse des
résultats de code informatique. Un paquetage R permet ensuite de
corriger le style de codage et la documentation de scripts dans ce
langage.

Roger est développé et utilisé activement pour la correction à 
l'[École d'actuariat](https://act.ulaval.ca) de
l'[Université Laval](https://ulaval.ca).

# Caractéristiques

Le système repose sur le concept de critères de correction tels que ceux du
[programme d'informatique de Arkansas State University](https://wiki.cs.astate.edu/index.php?title=Computer_Science_Program_Grading_Criteria&oldid=10507).
Roger se charge de la correction des critères objectifs comme la
justesse des résultats, le style de codage ou la présence d'éléments
de documentation (l'évaluation de leur qualité et de leur pertinence
en revient aux humains).

- **Multiplateforme** --- le cœur du système est basé sur des
  procédures d’interpréteur de commandes (*shell scripts*) et des
  outils standards Unix. (Requiert [MSYS2](https://msys2.org) ou [Git
  Bash](https://git-scm.com/download/win) sous Windows.)
  
- Prise en charge exhaustive des travaux hébergés et remis via des
  **dépôts Git** --- clonage en lot des dépôts; validation et
  correction de la structure des dépôts; publication des résultats
  directement dans les dépôts.
  
- Système de correction polyvalent basé sur des résultats de **tests
  unitaires** --- vous fournissez les tests, Roger se charge de
  les exécuter et de compiler les résultats.
  
- Grande **flexibilité** dans le nombre de fichiers à corriger, le nombre
  de caractéristiques à corriger pour chaque fichier et le nombre de
  critères de correction pour chaque caractéristique --- vous
  concevez la «carte de correction» à l'aide d'une syntaxe simple
  (similaire à [YAML](https://fr.wikipedia.org/wiki/YAML)), Roger
  exécute.
  
- Validation des travaux --- le système comprend un **script de
  validation** que les étudiants peuvent utiliser pour vérifier la
  validité de leur travail avant de le remettre. Évite bien des pleurs
  et des grincements de dents.

- Correction intégrée de scripts R, de documents R&nbsp;Markdown,
  d'applications Shiny et de procédures d'interpréteur de commandes.
  
- Vérification de l'intégrité de chaque fichier: nom, présence de
  données, codage de caractères, validité du code informatique.
  
- **Multilingue** --- l'intégralité du système fonctionne
  actuellement en français et en anglais.

# Composantes du système

Roger est actuellement formé de deux composantes indépendantes.

- [`roger-base`]({{< relref path="download.md" lang="fr" >}}) fournit
  les procédures d’interpréteur de commandes Bash à la base du
  système.
  
- Le paquetage R [**roger**]({{< param cran >}}) fournit une interface
  pour les procédures de `roger-base`, ainsi que les outils requis
  pour corriger le style de codage et la documentation dans les
  scripts R.

# Auteurs

- [David Beauchemin](https://github.com/davebulaval): idéation et
  premiers scripts de correction.

- Samuel Fréchette: interface R des scripts de correction.

- [Vincent Goulet](https://vigou3.gitlab.io/fr): procédures
  d’interpréteur de commandes; mainteneur du projet.
  
- Jean-Christophe Langlois: outils d'analyse statique du paquetage R.

# Licence

Roger est un logiciel libre distribué sous licence [GNU
GPL](https://www.gnu.org/licenses/gpl-3.0.html) version 2 ou
ultérieure.

# Contribuer

Les contributions sont bienvenues pour ajouter des fonctionnalités au
système, étendre sa portée à d'autres langages de programmation,
traduire le système dans une autre langue ou, tout simplement,
pour rapporter un bogue. [Rejoignez-nous dans
GitLab](https://gitlab.com/roger-project).
